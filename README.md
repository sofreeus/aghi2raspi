Pi Fight
========

This!
-----

![Pi Fight](pi-fight2.jpg)

Not this!
---------

![Pie Fight](piefight.jpg)

Welcome to SFS' Pi Fight event. We will (at least) compile Descent (D1X-Rebirth) on our Raspberry Pi's and play a LAN game of that. We _might_ go on from there to try:

 * Frozen Bubble
 * Minecraft Pi
 * OpenTTD
 * LBreakout2
 * Battle for Wesnoth

There is _no_ shortage of good games available for Linux. Here is a [short list][1].
  [1]: games_on_linux.text

Here are some [helpful instructions][2] for compiling D1X-Rebirth:
  [2]: how_to_compile_descent_on_pi.text

## notes
A flashing multi-colored box in the upper-right corner of the screen means the Pi isn't getting enough power. Try another power-supply and/or another cable.

glxgears is in mesa-utils and a few copies of glxgears may make an intermittent power problem more consistent

remember overscan in raspi-config

remember memory split in raspi-config
