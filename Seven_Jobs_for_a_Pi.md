# 7 things to do with a Pi

+ learn programming (That's what they made it for!)
+ emulator: MAME
+ Nextcloud/Samba/NFS file-server
+ media center: Kodi / OpenELEC
+ learn robotics!
 https://www.amazon.com/Yahboom-Professional-Programming-Electronic-Compatible/dp/B07KRVBGQM
+ Pi-Hole Ad-blocker
+ Just about *anything* you can use Linux for, works on a Pi.
