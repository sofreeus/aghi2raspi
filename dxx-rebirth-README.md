Do the steps in "download and compile" first!


## Extra challenges in Descent:

1. Find the dot-folders for d1x and d2x in your home-directory
2. Move the hogs to it, or replace them with the content from the full game.
3. mv MISSIONS/ missions/

Move d1x-rebirth and d2x-rebirth to a bin/

Downloads some add-on levels.

Record a demo.

## Troubleshooting:

In practice it has been found that a lot of the initial problems running Descent can be overcome by increasing the amount of shared memory the pi assigns to graphics. The steps below increase the amount of shared memory to 128MB.

1. From the command line run 'sudo raspi-config'
2. Select <advanced> from the menu
3. Select Shared Memory / Memory Split
4. Enter 128
5. Select Save.
6. Select Apply.
7. This will give you the option to Reboot Now or Later. If you 
   select later, you will not be able to take advantage of the
   increase in memory until you manually restart the Pi.
dxx-rebirth-download-and-build
dxx-rebirth-get-demo-content
dxx-rebirth-get-full-content
dxx-rebirth-README.md
