How to play a Jason Rohrer game
===============================
(on Linux, of course)

His games are shipped as source for unix (linux).
To meet the deps for compiling Transcend, on Debian, I did this:
sudo apt-get update && sudo apt-get -y dist-upgrade \
         && sudo apt-get -y install build-essential scons \
         openssh-server \
         libsdl1.2debian libsdl-mixer1.2 libsdl-mixer1.2-dev \
         libsdl-image1.2 libsdl-image1.2-dev \
         libsdl-ttf2.0-0 libsdl-ttf2.0-dev \
         libsdl-net1.2 libsdl-net1.2-dev \
         libphysfs-dev freeglut3\* \
         libxmu-dev libxi-dev

# Note: My Debian box was in VirtualBox with extensions, and as soon as I turned on 3D acceleration, the game would no longer launch. Turned it back off, and the game runs again.

# Transcend didn't compile
# Cultivation didn't compile
# Passage worked perfectly
